環境設定

------------------

# *Mac*
## 参考文献
### 主要
本README.mdの大部分は下記を参考にしています。

[2016版 OS毎python環境構築法決定版](http://qiita.com/y__sama/items/5b62d31cb7e6ed50f02c)

[HomebrewのインストールからpyenvでPythonのAnaconda環境構築までメモ](http://qiita.com/oct_itmt/items/2d066801a7464a676994)
### 補足
[anaconda2と3の共存](http://blogs.yahoo.co.jp/tsukada816/39224963.html)

[pyenvでバージョンの異なるPythonやAnacondaを共存させる](http://estuarine.jp/2016/01/pyenv/)
## 概要
「Homebrew」「pyenv」「anaconda」をインストールする。
「anaconda」はPythonの数値計算環境を構築するために様々なパッケージをまとめた無料のディストリビューションである。
「anaconda」をインストールすることで、scikit-learn並びに本勉強会で必要なライブラリをすべてインストールできる。
「Homebrew」「pyenv」は、現状のpython実行環境を壊さないための前準備（環境設定）です。
## 1. Homebrewがインストール済か確認
ターミナルを立ち上げ、下記コマンドを実行する
```Terminal
$ brew --version  
```
Homebrew 0.9.5 などと出る方はインストール済なので，3.へ
## 2. Homebrewのインストール
Homebrewがインストールされていない方
```Terminal
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## 3. Homebrew環境のアップデート
```Terminal
$ brew update && brew upgrade
```

## 4. pyenvのインストール
・bashを使っている場合
```Terminal
$ brew install pyenv
$ echo 'export PYENV_ROOT="${HOME}/.pyenv"' >> ~/.bash_profile
$ echo 'export PATH="${PYENV_ROOT}/bin:$PATH"' >> ~/.bash_profile
$ echo 'eval "$(pyenv init -)"' >> ~/.bash_profile

$ exec $SHELL -l
```

## 5. Python (Anaconda) のインストール
Python version 3系のanacondaをインストールする（とりあえず、下記のバージョンをインストールすることにする）
```Terminal
$ pyenv install anaconda3-4.1.0
```
また、下記コマンドでインストール可能であるバージョンを調べることができます
```Terminal
$ pyenv install -l | grep anaconda
(output)
  ....
  anaconda2-4.1.0
  anaconda2-4.1.1
  ....
  anaconda3-4.0.0
  anaconda3-4.1.0
```
anaconda2がpython2系、anaconda3がpython3系という意味です。
上のインストールコマンドの中で、「anaconda3-4.1.0」を別のものに置き換えれば、別の環境をインストールできます。
複数の環境をインストールでき、6.の手順で環境を切り替えることができます。
（「anaconda2-4.1.0」「anaconda2-4.1.1」「anaconda3-4.0.0」を同時にインストールでき、6.で切り替えることができる。）
## 6. pyenvによりpythonの実行環境を切り替える
```Terminal
$ pyenv global anaconda3-4.1.0
```
下記コマンドを実行した際、*の位置がanacondaの隣にあれば成功です。
```Terminal
$ pyenv versions
   system (set by /Users/xxxxxx/.pyenv/version)
*  anaconda3-4.1.0
```
下記コマンドで、元々のシステムのpython実行環境に戻すこともできます。
```Terminal
$ pyenv global system
```
## 7.確認
```Terminal
$ python --version
```
```
Python 3.5.1 :: Anaconda 4.1.0 (x86_64)
```
のように出ればOKです。
## 8.「jupyter Notebook」を立ち上げる
- ターミナルを立ち上げる
- 任意のディレクトリに「cd」で移動する
- 下記コマンドで「jupyter Notebook」を立ち上げる
```Terminal
$ jupyter notebook &
```
## 9.「jupyter Notebook」上で簡単なコードを書き、実行する
- 右の「New」⇒「python [Root]」をクリックする
- カーソルが点滅している箇所に下記を入力する

```JupyterNotebook
a=1
b=1
a+b
```

- 実行ボタン（■の左側にあるボタン）をクリックし、実行する
- Outputに2と表示されることを確認する
- ファイル名を変更したければ、上の「Untiled」をクリックし、名前を変更する

## 10.おまけ
http://qiita.com/y__sama/items/5b62d31cb7e6ed50f02c#pyenv

------------------

# *windows*
## 概要
「anaconda」をインストールする。
## 1. ダウンロードサイトから「anaconda」のpython 3.5 versionをダウンロードする
https://www.continuum.io/downloads#_windows
## 2. 「anaconda」をインストールする
- exeを実行して指示に従ってインストールする
    - すべてデフォルトのままでOK
## 3.「jupyter Notebook」を立ち上げる
- 左下の「windowsマーク」クリック⇒「すべてのアプリ」クリック⇒「Anaconda3」の中で「jupyter Notebook」をクリックする
    - ブラウザ上に「jupyter Notebook」が立ち上がる
## 4.「jupyter Notebook」上に作業用フォルダをつくる
- 右の「New」⇒「Folder」をクリックする
- 新たに作成された「Untitled Folder」の左のチェックボックスをクリックし、上の「Rename」をクリックする
- 任意の名前に変更する
- 同フォルダをクリックし、チェンジディレクトリする
## 5.「jupyter Notebook」上で簡単なコードを書き、実行する
- 右の「New」⇒「python [conda root]」をクリックする
- カーソルが点滅している箇所に下記を入力する

```JupyterNotebook
a=1
b=1
a+b
```

- 実行ボタン（■の左側にあるボタン）をクリックし、実行する
- Outputに2と表示されることを確認する
- ファイル名を変更したければ、上の「Untiled」をクリックし、名前を変更する

## 6. その他準備
7-zipをダウンロードする

## 7. おまけ
anacondaはeclipseと連携できるそうです
http://www.gixo.jp/blog/3861
